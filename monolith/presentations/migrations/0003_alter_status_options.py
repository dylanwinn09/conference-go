# Generated by Django 4.0.3 on 2023-03-23 01:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0002_alter_status_options_alter_presentation_status'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='status',
            options={'ordering': ('id',), 'verbose_name_plural': 'statuses'},
        ),
    ]
